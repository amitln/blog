var express = require('express');
var router = express.Router();

var mysql = require('mysql');
var db = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'blog',
  debug: false
});


const courses = [
  { id: 1, name: 'course1' },
  { id: 2, name: 'course2' },
  { id: 3, name: 'course3' },
];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/about', (req, res) => {
  res.render('about');
});

router.get('/contact', (req, res) => {
  res.render('contact');
});

//check database connection
router.get('/testconnection', function(req, res, next) {
  if(db != null) {
    res.send('connected successfully!!!')
  }
  else {
    res.send('connection failed!!!')
  }
});


//fetch users
router.get('/users', function(req, res, next) {
  db.query('SELECT * FROM user', function(err, rs) {
    res.render('users', { users: rs});
  });
});


//show user form
router.get('/add', function(req, res, next) {
  res.render('add-user', {user: {} });
});


//add user data
router.post('/add', function(req, res, next) {
  db.query('INSERT INTO user SET ?', req.body, function(err, rs) {
    res.redirect('/users');
  });
});


//delete user data
router.get('/delete', function(req, res, next) {
  db.query('DELETE FROM user WHERE id = ?', req.query.id, function(err, rs) {
    res.redirect('/users');
  });
});

router.get('/edit', function(req, res, next) {
  db.query('SELECT * FROM user WHERE id = ?', req.query.id, function(err, rs) {
    res.render('add-user', { user: rs[0]});
  });
});


router.post('/edit', function(req, res, next) {
  var param = [
    req.body,
    req.query.id
  ]
  db.query('UPDATE user SET ? WHERE id = ?', param, function(err, rs) {
    res.redirect('/users');
  });
});


router.get('/check', function(req, res, next) {
  res.send(courses);
});

router.get('/check/:id', function(req, res, next) {
  const course = courses.find(c => c.id === parseInt(req.params.id));
  if(!course) res.status(404).send('The course with the given id not found!!');
  res.send(course);
});

module.exports = router;
